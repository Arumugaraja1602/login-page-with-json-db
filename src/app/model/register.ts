export interface Register{
    username ?: String;
    password ?: String;
    emailId ?: String;
    mobileNo ?: Number;
}