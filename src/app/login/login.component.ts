import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from "../service/user.service";
import { Login } from "../model/login";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    emailId: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });
  public login: Login = {};
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  formSubmit() {
    this.login.emailId = this.loginForm.value.emailId;
    this.login.password = this.loginForm.value.password;
    this.userService.createLogin(this.login).subscribe((res) => {
      if (res.message === 'Authorized user' && res.status === 200) {
        if (res.token !== null) {
          localStorage.setItem('token', res.token);
          this.router.navigate(['/dashboard']);
        }else if(res.message === 'Invalid Password'){
          alert('Invalid Password');
        }
      } else if(res.message === 'Unauthorized user!! Register First'){
        alert('Unauthorized user!! Register First');
        this.router.navigate(['/register']);
      } else {
        alert('Parameters are missing!!');
      }
    })



    // this.userService.getRegister().subscribe((res) => {
    //   let registeredUser = [...res];
    //   const filterdata = registeredUser.filter((obj: any) =>
    //     obj.emailId == this.login.emailId &&
    //     obj.password == this.login.password
    //   );
    //   console.log('filterdata:', filterdata);
    //   if (filterdata.length !== 0) {
    //       localStorage.setItem('isLoggedIn', 'true');
    //       this.loginForm.reset();
    //       this.router.navigate(['/dashboard']);
    //   }
    //   else {
    //     this.router.navigate(['/register']);
    //     this.loginForm.reset();
    //   }
    // })

  }

}
