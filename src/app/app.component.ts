import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Loginpage';

  isHidden = true;

  onActivate() {
    // console.log(window.location.pathname);
    if(window.location.pathname === '/login' || window.location.pathname === '/register'){
      this.isHidden = false;
    }
    else{
      this.isHidden = true;
    }
  }

}
