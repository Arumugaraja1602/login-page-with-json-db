import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { catchError } from "rxjs/operators";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // public baseUrl = "http://localhost:3000";  // Angular Json db port
  public baseUrl = environment.baseUrl;
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  public userLogin = false;

  constructor(private http: HttpClient) {
   }

  /* 
  //  setUser(){
  //   this.userLogin = true;
  //  }

  //  getUser(){
  //   return this.userLogin;
  //  }

    getUserDetails(): Observable<any> 
    {
      return this.http.get(`${this.baseUrl}/login`);
    }
  */
 
    // Error handling
    errorMgmt(error: HttpErrorResponse) {
      let errorMessage = '';
      if (error.error instanceof ErrorEvent) {
        // Get client-side error
        errorMessage = error.error.message;
      } else {
        // Get server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      console.log(errorMessage);
      return throwError(errorMessage);
    }

    // Register
    createRegister(data: any): Observable<any>{
      const url = `${this.baseUrl}/register/create`;
      return this.http.post(url,data).pipe(catchError(this.errorMgmt));
    }

    getRegister():Observable<any>{
      const url = `${this.baseUrl}/register/getAll`;
      return this.http.get(url).pipe(catchError(this.errorMgmt));
    }

    // Login
    createLogin(data: any):Observable<any>{
      const url= `${this.baseUrl}/login/create`;
      return this.http.post(url, data).pipe(catchError(this.errorMgmt));
    }

    getLogin(): Observable<any>{
      const url=`${this.baseUrl}/login/getAll`;
      return this.http.get(url).pipe(catchError(this.errorMgmt));
    }
}

