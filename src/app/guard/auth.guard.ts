import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from "../service/user.service";

@Injectable({
  providedIn: 'root'
})


export class AuthGuard implements CanActivate{

  constructor(private service: UserService, private router: Router){

  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      if(localStorage.getItem('token')){
        this.router.navigate(['/dashboard']);
        return false;
      }else{
        return true;
      }

  }
}
