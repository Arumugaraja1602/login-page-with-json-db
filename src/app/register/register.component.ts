import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Register } from "../model/register";
import { UserService } from "../service/user.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  register: Register = {}

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  public registerForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    emailId: new FormControl('', Validators.required),
    mobileNo: new FormControl('', Validators.required)
  });

  formSubmit() {
    this.register.username = this.registerForm.value.username;
    this.register.password = this.registerForm.value.password;
    this.register.emailId = this.registerForm.value.emailId;
    this.register.mobileNo = this.registerForm.value.mobileNo;
    this.userService.createRegister(this.register).subscribe((res)=>{
      if(res.message === 'User Register Successfully'){
        alert(res.message);
        this.router.navigate(['/login']);
      } else {
        alert(res.message);
        this.registerForm.reset();
        
      }
    })

    // this.userService.getRegister().subscribe((res) => {
    //   const registeredUser = [...res];
    //   const filterdata = registeredUser.filter((obj: any) =>
    //     obj.username === this.register.username &&
    //     obj.password === this.register.password
    //   );
    //   console.log('filterdata:', filterdata);
    //   if (filterdata.length !== 0) {
    //     console.log('Already User is registered');
    //     this.registerForm.reset();
    //   }
    //   else {
    //     this.userService.createRegister(this.register).subscribe((res) => {
    //       console.log('Register Create:', res);
    //       this.router.navigate(['/login']);
    //       this.registerForm.reset();
    //     })
    //   }
    // })
  }

}
